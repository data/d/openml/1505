# OpenML dataset: tamilnadu-electricity

https://www.openml.org/d/1505

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: [K.Kalyani](kkalyanims@gmail.com)T.U.K Arts College,Karanthai,Thanjavur  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Tamilnadu+Electricity+Board+Hourly+Readings) - 2013    
**Please cite**: None

### Description

Tamilnadu Electricity Board Hourly Readings dataset. 

### Source
```
K.Kalyani ,kkalyanims '@' gmail.com,  
T.U.K Arts College,Karanthai,Thanjavur.
```

### Data Set Information

Real-time readings were collected from residential, commercial, industrial and agriculture to find the accuracy consumption in Tamil Nadu, around Thanajvur. 

**Note**: the attribute Sector was removed from original source since it was constant to all instances.

### Attribute Information:
```
1 - ForkVA (V1) : real
2 - ForkW (V2) : real
3 - ServiceID (V3): factor

4 - Type (Class): 
- Bank  
- AutomobileIndustry 
- BpoIndustry   
- CementIndustry   
- Farmers1   
- Farmers2   
- HealthCareResources 
- TextileIndustry 
- PoultryIndustry 
- Residential(individual)  
- Residential(Apartments)    
- FoodIndustry   
- ChemicalIndustry   
- Handlooms   
- FertilizerIndustry   
- Hostel   
- Hospital   
- Supermarket   
- Theatre   
- University

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1505) of an [OpenML dataset](https://www.openml.org/d/1505). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1505/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1505/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1505/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

